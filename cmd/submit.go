package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// submitCmd represents the submit command
var submitCmd = &cobra.Command{
	Use:   "submit",
	Short: "Submit metadata to the aggregated MotW Library.",
	Long: `Submit all the library's metadata to the aggregated Memory of the World
Library (https://library.memoryoftheworld.org).`,
	// Args: OnlyProfileArgument,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(args)
	},
}

func init() {
	rootCmd.AddCommand(submitCmd)
	submitCmd.PersistentFlags().StringP("librarian", "l", "", "Librarian's name.")
	submitCmd.PersistentFlags().BoolP("remove-library", "", false, "Remove PROFILE's library from the MotW Library.")
	CustomHelpOutput(submitCmd)
}
