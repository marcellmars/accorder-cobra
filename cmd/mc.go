/*
Copyright © 2021

*/
package cmd

import (
	miniocmd "github.com/minio/mc/cmd"
	"github.com/spf13/cobra"
	"os"
)

// mcCmd represents the mc command
var mcCmd = &cobra.Command{
	Use:   "mc",
	Short: "A proxy for `mc` minio client responsible for upload & download`.",
	Run: func(cmd *cobra.Command, args []string) {
		// rgs := []string{"", "-C", ConfigMinioDir()}
		// rgs := []string{"", "-C", ConfigMinioDir()}
		os.Setenv("MC_HOST_minio.memoryoftheworld.org", "https://klemo:U9@?x$)Kdoq15)J~@minio.memoryoftheworld.org")
		rgs := []string{"", "-C", ConfigMinioDir(), "ls", "minio.memoryoftheworld.org/klemo"}
		rgs = append(rgs, args...)
		miniocmd.Main(rgs)
	},
}

func init() {
	mcCmd.DisableFlagParsing = true
	rootCmd.AddCommand(mcCmd)
}
