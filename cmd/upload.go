package cmd

import (
	"fmt"
	"os"

	miniocmd "github.com/minio/mc/cmd"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// uploadCmd represents the upload command
var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "Upload local Calibre library to the MotW server.",
	Long: `Upload local Calibre library to the Memory of the World server.
It will take care of the differences so files already at the server
will not be uploaded again.

Every time the directory path and/or librarian is provided it is saved in
configuration file for the future use (therefore: 'accorder upload SESSION'
should be enough for the next successful upload).`,
	Args: cobra.ExactArgs(1),
	PreRun: func(cmd *cobra.Command, args []string) {
		session := args[0]

		for vipFlag, cliFlag := range map[string]string{
			"server_upload":  "server",
			"library_uuid":   "username",
			"library_secret": "password",
			"bucket_upload":  "bucket",
			"local_upload":   "directory",
			"librarian_name": "librarian",
		} {
			viper.BindPFlag(fmt.Sprintf("%s.%s", session, vipFlag), cmd.Flags().Lookup(cliFlag))
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		session := args[0]

		MissingRequiredFlags(
			map[string]string{
				"server_upload":  "server",
				"library_uuid":   "username",
				"library_secret": "password",
				"bucket_upload":  "bucket",
				"local_upload":   "directory",
			},
			session,
			cmd,
		)

		server := ViperValue(session, "server_upload")
		// username in upload context comes from library_uuid
		username := ViperValue(session, "library_uuid")
		// password in upload context comes from library_secret
		password := ViperValue(session, "library_secret")
		bucket := ViperValue(session, "bucket_upload")
		localDirectory := ViperValue(session, "local_upload")
		deleteResidue, _ := cmd.PersistentFlags().GetBool("delete-residue")
		verbose, _ := cmd.PersistentFlags().GetBool("verbose")

		os.Setenv(
			fmt.Sprintf("MC_HOST_%s", server),
			fmt.Sprintf("https://%s:%s@%s", username, password, server),
		)
		rgs := []string{"",
			"-C", ConfigMinioDir(),
			"mirror", "--overwrite",
		}

		if deleteResidue {
			rgs = append(rgs, "--remove")
		}

		if verbose {
			rgs = append(rgs, "--json")
		}

		rgs = append(rgs, localDirectory, fmt.Sprintf("%s/%s", server, bucket))

		miniocmd.Main(rgs)
	},
}

func init() {
	rootCmd.AddCommand(uploadCmd)

	uploadCmd.Flags().StringP("directory", "d", "", "A local directory to be uploaded.")
	uploadCmd.Flags().StringP("username", "u", "", "Username.")
	uploadCmd.Flags().StringP("password", "p", "", "Password.")
	uploadCmd.Flags().StringP("bucket", "b", "", "A remote directory/bucket where to upload.")
	uploadCmd.Flags().StringP("server", "s", "minio.memoryoftheworld.org", "Server.")
	uploadCmd.Flags().BoolP("delete-residue", "", false, "Delete any remote files not present locally anymore.")
	uploadCmd.Flags().BoolP("verbose", "v", false, "Verbose log.")
	uploadCmd.Flags().StringP("librarian", "l", "", "Librarian's name.")

	CustomHelpOutput(uploadCmd)

	uploadCmd.Flags().MarkHidden("librarian")
}
