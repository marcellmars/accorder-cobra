package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/kirsle/configdir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func MissingRequiredFlags(flags map[string]string, session string, cmd *cobra.Command) {
	exit := false
	for vipFlag, cliFlag := range flags {
		if ViperValue(session, vipFlag) == "" {
			fmt.Printf("ERROR: A flag --%s is missing for SESSION `%s` to work.\n", cliFlag, session)
			exit = true
		}
	}

	if exit {
		fmt.Println("~ ~ ~ ~")
		cmd.Usage()
		os.Exit(1)
	}

	if cmd.Flags().NFlag() > 0 {
		viper.WriteConfig()
	}
}

func CustomHelpOutput(cmd *cobra.Command) {
	cmd.Flags().SortFlags = false
	cmd.SetHelpTemplate(HelpTemplate)
	cmd.SetUsageTemplate(UsageTemplate)
}

func ViperSettingsPrint() {
	vipAll, err := json.MarshalIndent(viper.AllSettings(), "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(vipAll))
}

func CliFlagValue(cmd *cobra.Command, key string) (value string) {
	value, _ = cmd.Flags().GetString(key)
	return
}

func ViperValue(session, key string) string {
	return viper.GetString(fmt.Sprintf("%s.%s", session, key))
}

func CheckProfile(cmd *cobra.Command, args []string) error {
	hasProfile := false
	// fmt.Println("VIPER:", viper.AllSettings())
	for k, v := range viper.AllSettings() {
		fmt.Println("VIPER KEY/VALUE:", k, v)
		if k == args[0] {
			hasProfile = true
		}
	}

	if !hasProfile {
		return fmt.Errorf("PROFILE %s should be added to the configuration file.\n", args[0])
	}
	return fmt.Errorf("MEH! %s should be added to the configuration file.\n", args[0])
}

func ConfigBaseDir() string {
	return configdir.LocalConfig("akkorder")
}

func ConfigMinioDir() string {
	return filepath.Join(ConfigBaseDir(), "minio")
}

func initConfigPaths() {
	err := configdir.MakePath(ConfigBaseDir()) // Ensure it exists.
	if err != nil {
		panic(err)
	}
	os.MkdirAll(filepath.Join(ConfigBaseDir(), "minio"), 0777)
}
