module accorder

go 1.16

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de // indirect
	github.com/beevik/etree v1.1.0
	github.com/karrick/godirwalk v1.16.1
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/minio/mc v0.0.0-20211116163708-d0c62eb584e5
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.9.0
)
